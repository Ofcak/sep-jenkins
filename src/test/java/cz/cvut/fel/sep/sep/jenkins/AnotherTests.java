/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.fel.sep.sep.jenkins;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 *
 * @author Mira
 */

@Category(IntegrationTest.class)
public class AnotherTests {
    
    public AnotherTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Main.main(args);
        // TODO review the generated test code and remove the default call to fail.
        
        //fail("The test case is a prototype.");
    }

    /**
     * Test of countSumFromFibonaciLastNum method, of class Main.
     */
    
    static public void testCountSumFromFibonaciLastNum(int number, int expResult){
        System.out.println("countSumFromFibonaciLastNum for number: "+number);
        int result = Main.countSumFromFibonaciLastNum(number);
        assertEquals(expResult, result);
    }
    
    /**
     * Tests negative integer.
     */
    @Test
    public void testCountSumFromFibonaciLastNum_negative() {
        testCountSumFromFibonaciLastNum(-5, 0);
    }

    /**
     * Tests bug positive integer.
     */
    @Test
    public void testCountSumFromFibonaciLastNum_bigNumber() {
        testCountSumFromFibonaciLastNum(40, 165580140);
    }
}