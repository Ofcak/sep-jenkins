/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.fel.sep.sep.jenkins;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mira
 */
//@TestGroup("integration")
public class SmokeTest {
    
    public SmokeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test positive integer - basic functionality.
     */
    @Test
    public void testCountSumFromFibonaciLastNum_positive() {
        int number = 5;
        int expResult = 7;
        AnotherTests.testCountSumFromFibonaciLastNum(number, expResult);
    }
    
    /**
     * Tests zero value.
     */
    @Test
    public void testCountSumFromFibonaciLastNum_zero() {
        AnotherTests.testCountSumFromFibonaciLastNum(0, 0);
    }
}
